package rpc_test

import (
	"context"
	"fmt"
	"gitee.com/lion-yie/keyauth/apps/token"
	mcenter "github.com/infraboard/mcenter/client/rpc"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitee.com/lion-yie/keyauth/client/rpc"
)

// 测试keyauth的一个客户端sdk，ValidateToken的操作
func TestValidateToken(t *testing.T) {
	should := assert.New(t)

	// gRPC服务配置
	conf := mcenter.NewDefaultConfig()
	conf.Address = os.Getenv("MCENTER_ADDRESS")
	conf.ClientID = os.Getenv("MCENTER_CLINET_ID")
	conf.ClientSecret = os.Getenv("MCENTER_CLIENT_SECRET")

	fmt.Printf("addr:%s\nclient_id:%s\nclient_secret:%s\n",
		conf.Address,
		conf.ClientID,
		conf.ClientSecret,
	)

	// 传递Mcenter配置,客户端通过Mcenter进行搜索,New一个用户中心的客户端
	keyauthClient, err := rpc.NewClient(conf)

	// 使用SDK 调用 keyauth 进行凭证的校验
	//keyauthClient.Token().ValidateToken()

	if should.NoError(err) {
		// 走的是grpc的服务
		resp, err := keyauthClient.Token().ValidateToken(
			context.Background(),
			token.NewValidateTokenRequest("NtMXipup3CzkWs9orgymFkJc"),
		)
		should.NoError(err)
		fmt.Println(resp)
	}
	fmt.Printf("err is %s\n", err)
}

func init() {
	// 提前加载好 mcenter客户端,resolver需要使用
	err := mcenter.LoadClientFromEnv()
	if err != nil {
		panic(err)
	}
}
