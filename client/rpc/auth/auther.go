package auth

import (
	"gitee.com/lion-yie/keyauth/apps/token"
	"github.com/infraboard/mcube/logger"
	"github.com/infraboard/mcube/logger/zap"
)

func NewKeyauthAuther(auth token.ServiceClient) *KeyauthAuther {
	return &KeyauthAuther{
		auth: auth,
		log:  zap.L().Named("http.auther"),
	}
}

type KeyauthAuther struct {
	log  logger.Logger
	auth token.ServiceClient
}
