package auth

import (
	"gitee.com/lion-yie/keyauth/apps/token"
	"gitee.com/lion-yie/keyauth/common/utils"
	"github.com/emicklei/go-restful/v3"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/http/label"
	"github.com/infraboard/mcube/http/response"
)

func (a *KeyauthAuther) RestfulAuthHandlerFunc(
	req *restful.Request,
	resp *restful.Response,
	chain *restful.FilterChain,
) {
	// 获取路由装饰信息
	meta := req.SelectedRoute().Metadata()
	a.log.Debug(meta)

	// 获取meta信息,get,判断是否开启认证
	var isAuthEnable bool
	if authV, ok := meta[label.Auth]; ok {
		switch v := authV.(type) {
		case bool:
			isAuthEnable = v
		case string:
			isAuthEnable = v == "true"
		}

		if isAuthEnable {
			// 1.认证中间件,获取Token
			accessToken := utils.GetTokenFromHeader(req.Request)

			// 2.到用户中心验证token合法性,依赖于用户中心的Grpc client
			validateReq := token.NewValidateTokenRequest(accessToken)
			tk, err := a.auth.ValidateToken(req.Request.Context(), validateReq)
			if err != nil {
				response.Failed(resp.ResponseWriter, exception.NewUnauthorized(err.Error()))
				return
			}

			// 将获取的token对象放入到restful包装的ctx中去，后续可以通过req.Attribute()取出
			req.SetAttribute("token", tk)
			//if tkNew, ok := req.Attribute("token").(*token.Token); !ok {
			//	return
			//}
		}
	}

	// chain 用于将请求flow下去
	chain.ProcessFilter(req, resp)
}
