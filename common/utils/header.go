package utils

import (
	"net/http"
	"strings"
)

func GetTokenFromHeader(r *http.Request) (accessToken string) {
	auth := r.Header.Get("Authorization")
	al := strings.Split(auth, " ")
	if len(al) > 1 {
		accessToken = al[1]
	} else {
		// 兼容 Authorization <token>
		accessToken = auth
	}
	return accessToken
}
