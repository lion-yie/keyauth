package utils_test

import (
	"gitee.com/lion-yie/keyauth/common/utils"
	"testing"
	"time"
)

func TestHash(t *testing.T) {
	v1 := utils.Hash("abc")
	t.Log(v1)

	v2 := utils.Hash("1")
	t.Log(v2)

	v3 := utils.Hash("12123123dfa!~dfasd我发了附近阿拉基")
	t.Log(v3)

	v4 := utils.Hash("")
	t.Log(v4)
}

func TestHashPassword(t *testing.T) {
	v1 := utils.HashPassword("abc")
	t.Log(v1)

	v2 := utils.HashPassword("abc")
	t.Log(v2)

	v3 := utils.HashPassword("1")
	t.Log(v3)

	v4 := utils.HashPassword("12123123dfa!~dfasd我发了附近阿拉基")
	t.Log(v4)

	v5 := utils.HashPassword("")
	t.Log(v5)
}

func TestCheckPasswordHash(t *testing.T) {
	n1 := time.Now()
	ok := utils.CheckPasswordHash("abc", "$2a$14$hEYltERmQXTwVODPQGFc8uBubVcRnbo2aH2uwF3BH1p8QVpPf.OKm")
	n2 := time.Now()
	t.Logf("比对结果:%v,\n比对结果测试时间:%v\n", ok, n2.Sub(n1))
}
