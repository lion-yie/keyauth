package user

import (
	"github.com/rs/xid"
	"net/http"
	"time"

	"github.com/infraboard/mcube/http/request"
	pb_request "github.com/infraboard/mcube/pb/request"

	"github.com/go-playground/validator/v10"

	"gitee.com/lion-yie/keyauth/common/utils"
)

const (
	AppName = "user"
)

const (
	DefaultDomain = "default"
)

var (
	validate = validator.New()
)

// 保存Hash过后的password
func NewUser(req *CreateUserRequest) *User {
	return &User{
		Id:       xid.New().String(),
		CreateAt: time.Now().UnixMilli(),
		Data:     req,
	}
}

func (req *CreateUserRequest) Validate() error {
	return validate.Struct(req)
}

func NewUserSet() *UserSet {
	return &UserSet{
		Items: []*User{},
	}
}

func NewDefaultUser() *User {
	return &User{
		Data: &CreateUserRequest{},
	}
}

func (u *User) CheckPassword(password string) bool {
	return utils.CheckPasswordHash(password, u.Data.Password)
}

func (s *UserSet) Add(item *User) {
	s.Items = append(s.Items, item)
}

func NewCreateUserRequest() *CreateUserRequest {
	return &CreateUserRequest{
		Domain: DefaultDomain,
	}
}

func NewQueryUserRequestFromHTTP(r *http.Request) *QueryUserRequest {
	qs := r.URL.Query()

	return &QueryUserRequest{
		Page:     request.NewPageRequestFromHTTP(r),
		Keywords: qs.Get("keywords"),
	}
}

func NewDescribeUserRequestById(id string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy: DescribeBy_USER_ID,
		UserId:     id,
	}
}

func NewDescribeUserRequestByName(domain, name string) *DescribeUserRequest {
	return &DescribeUserRequest{
		DescribeBy: DescribeBy_USER_NAME,
		UserName:   name,
		Domain:     domain,
	}
}

func NewPutUserRequest(id string) *UpdateUserRequest {
	return &UpdateUserRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PUT,
		UpdateAt:   time.Now().UnixMicro(),
		Data:       NewCreateUserRequest(),
	}
}

func NewPatchUserRequest(id string) *UpdateUserRequest {
	return &UpdateUserRequest{
		Id:         id,
		UpdateMode: pb_request.UpdateMode_PATCH,
		UpdateAt:   time.Now().UnixMicro(),
		Data:       NewCreateUserRequest(),
	}
}

func NewDeleteUserRequestWithID(id string) *DeleteUserRequest {
	return &DeleteUserRequest{
		Id: id,
	}
}
