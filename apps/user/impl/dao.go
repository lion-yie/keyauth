package impl

import (
	"context"
	"fmt"
	"gitee.com/lion-yie/keyauth/apps/user"
	"github.com/infraboard/mcube/exception"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func newQueryUserRequest(r *user.QueryUserRequest) *queryUserRequest {
	return &queryUserRequest{
		r,
	}
}

type queryUserRequest struct {
	*user.QueryUserRequest
}

func (r *queryUserRequest) FindOptions() *options.FindOptions {
	pageSize := int64(r.Page.PageSize)
	skip := int64(r.Page.PageSize) * int64(r.Page.PageNumber-1)

	opt := &options.FindOptions{
		// 排序：order by create_at desc
		Sort: bson.D{
			{Key: "create_at", Value: -1},
		},
		// 分页
		Limit: &pageSize,
		Skip:  &skip,
	}

	return opt
}

// 过滤条件
func (r *queryUserRequest) FindFilter() bson.M {
	filter := bson.M{}
	//if r.Keywords != "" {
	//	filter["$or"] = bson.A{
	//		// 使用 . 来访问嵌套的json字段key值
	//		bson.M{"data.name": bson.M{"$regex": r.Keywords, "$options": "im"}},
	//		bson.M{"data.author": bson.M{"$regex": r.Keywords, "$options": "im"}},
	//	}
	//}
	return filter
}

// LIST,会做很多条件(分页,关键字,条件过滤,排序)
// 需要单独为其做过滤参数构建
func (s *service) query(ctx context.Context, req *queryUserRequest) (*user.UserSet, error) {
	resp, err := s.col.Find(ctx, req.FindFilter(), req.FindOptions())

	if err != nil {
		return nil, exception.NewInternalServerError("find user error, error is %s", err)
	}

	set := user.NewUserSet()
	// 循环
	for resp.Next(ctx) {
		ins := user.NewDefaultUser()
		if err := resp.Decode(ins); err != nil {
			return nil, exception.NewInternalServerError("decode user error, error is %s", err)
		}

		set.Add(ins)
	}

	// count
	count, err := s.col.CountDocuments(ctx, req.FindFilter())
	if err != nil {
		return nil, exception.NewInternalServerError("get user count error, error is %s", err)
	}
	set.Total = count

	return set, nil
}

// filter:过滤器,类似于MySQL的Where条件
// 调用Decode方法来进行 反序列化 bytes->object(通过BSON tag)
func (s *service) get(ctx context.Context, req *user.DescribeUserRequest) (*user.User, error) {

	filter := bson.M{}

	switch req.DescribeBy {
	case user.DescribeBy_USER_ID:
		filter["_id"] = req.UserId
	case user.DescribeBy_USER_NAME:
		filter["data.domain"] = req.Domain
		filter["data.name"] = req.UserName
	default:
		return nil, fmt.Errorf("unknow describe_by %s, support: %s", req.DescribeBy, user.DescribeBy_value)
	}

	ins := user.NewDefaultUser()
	// 反序列化
	if err := s.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("user %s not found", req)
		}

		return nil, exception.NewInternalServerError("find user %s error, %s", req, err)
	}

	return ins, nil
}
