package token

import (
	"fmt"
	"gitee.com/lion-yie/keyauth/apps/user"
	"gitee.com/lion-yie/keyauth/common/utils"
	"github.com/go-playground/validator/v10"
	"github.com/infraboard/mcube/exception"
	"time"
)

const (
	AppName = "token"
)

var (
	validate = validator.New()
)

func (req *IssueTokenRequest) Validate() error {
	switch req.GranteType {
	case GranteType_PASSWORD:
		if req.UserName == "" || req.Password == "" {
			return fmt.Errorf("password grant required username and password")
		}
	}
	return nil
}

func NewDefaultToken() *Token {
	return &Token{
		Data: &IssueTokenRequest{},
		Meta: map[string]string{},
	}
}

func NewDescribeTokenRequest(at string) *DescribeTokenRequest {
	return &DescribeTokenRequest{
		AccessToken: at,
	}
}

func NewToken(req *IssueTokenRequest, expiredDuration time.Duration) *Token {
	now := time.Now()
	expired := now.Add(expiredDuration)
	refreshExpired := now.Add(5 * expiredDuration)
	return &Token{
		AccessToken:           utils.MakeBearer(24),
		IssueAt:               now.UnixMilli(),
		Data:                  req,
		AccessTokenExpiredAt:  expired.UnixMilli(),
		RefreshToken:          utils.MakeBearer(32),
		RefreshTokenExpiredAt: refreshExpired.UnixMilli(),
	}
}

func (t *Token) Validate() error {
	// 判断Token是否过期
	if time.Now().UnixMilli() > t.AccessTokenExpiredAt { // 是一个时间戳,要跟当前时间比
		return exception.NewAccessTokenExpired("access token expired")
	}
	return nil
}

func (t *Token) IsRefreshTokenExpired() bool {
	// 判断refresh token是否过期
	if time.Now().UnixMilli() > t.RefreshTokenExpiredAt { // 是一个时间戳,要跟当前时间比
		return true
	}
	return false
}

// token 续期(延长一个生命周期)
func (t *Token) Renewal(expiredDuration time.Duration) {
	now := time.Now()
	tokenExpired := now.Add(expiredDuration)
	refreshExpired := now.Add(5 * expiredDuration)

	t.AccessTokenExpiredAt = tokenExpired.UnixMilli()
	t.RefreshTokenExpiredAt = refreshExpired.UnixMilli()
}

func NewIssueTokenRequest() *IssueTokenRequest {
	return &IssueTokenRequest{
		UserDomain: user.DefaultDomain,
	}
}

func NewValidateTokenRequest(at string) *ValidateTokenRequest {
	return &ValidateTokenRequest{
		AccessToken: at,
	}
}

func NewRevolkTokenRequest() *RevolkTokenRequest {
	return &RevolkTokenRequest{}
}
