package impl

import (
	"context"
	"fmt"
	"gitee.com/lion-yie/keyauth/apps/token"
	"gitee.com/lion-yie/keyauth/apps/user"
	"gitee.com/lion-yie/keyauth/common/utils"
	"github.com/infraboard/mcube/exception"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var (
	AUTH_ERROR = "user or password not correct"
)

var (
	DefaultTokenDuration = 10 * time.Minute
)

func (s *service) IssueToken(ctx context.Context, req *token.IssueTokenRequest) (*token.Token, error) {
	if err := req.Validate(); err != nil {
		return nil, exception.NewBadRequest("validate issue token error,%s", err)
	}

	// 根据不同授权模型来做不同的验证
	switch req.GranteType {
	case token.GranteType_PASSWORD:
		// 1. 获取用户对象(User object)
		descReq := user.NewDescribeUserRequestByName(req.UserDomain, req.UserName)
		u, err := s.user.DescribeUser(ctx, descReq)
		if err != nil {
			if exception.IsNotFoundError(err) {
				// 401
				return nil, exception.NewUnauthorized(AUTH_ERROR)
			}
			return nil, err
		}
		// 2. 校验用户密码是否正确
		s.log.Debug(u)
		if ok := u.CheckPassword(req.Password); !ok {
			// 401
			return nil, exception.NewUnauthorized(AUTH_ERROR)
		}

		// 3. 颁发一个Token
		// 4. rfc: Bearer 字符串:  Header: Authorization: bear
		tk := token.NewToken(req, DefaultTokenDuration)

		// 5. 脱敏
		tk.Data.Password = ""

		// 6.入库持久化
		if err = s.save(ctx, tk); err != nil {
			return nil, err
		}

		return tk, nil
	default:
		return nil, fmt.Errorf("grant type %s not implemented", req.GranteType)

	}

	return nil, status.Errorf(codes.Unimplemented, "method IssueToken not implemented")
}
func (s *service) RevolkToken(ctx context.Context, req *token.RevolkTokenRequest) (*token.Token, error) {
	// 1. 获取access_token
	tk, err := s.get(ctx, req.AccessToken)
	if err != nil {
		return nil, err
	}

	// 2. 检查refresh_token是否匹配
	if tk.RefreshToken != req.RefreshToken {
		return nil, exception.NewBadRequest("refresh token not conrrect")
	}

	// 3. 删除
	if err = s.deleteToken(ctx, tk); err != nil {
		return nil, err
	}
	return tk, nil
}
func (s *service) ValidateToken(ctx context.Context, req *token.ValidateTokenRequest) (*token.Token, error) {
	// 1. 获取access_token
	tk, err := s.get(ctx, req.AccessToken)
	if err != nil {
		return nil, err
	}

	// 2. 校验token的合法性
	if err = tk.Validate(); err != nil {
		// 2.1 如果token过期
		if utils.IsAccessTokenExpiredError(err) {
			// 2.2 判断refresh token是否过期
			if tk.IsRefreshTokenExpired() {
				return nil, exception.NewRefreshTokenExpired("refresh token expired")
			}
			// 2.3 如果refresh没过期，可以延长其过期时间(类似于update操作)
			tk.Renewal(DefaultTokenDuration)
			if err := s.update(ctx, tk); err != nil {
				return nil, err
			}
			// 返回续约后的token
			return tk, nil
		}
		return nil, err
	}

	return tk, nil
}

func (s *service) DescribeToken(ctx context.Context, req *token.DescribeTokenRequest) (*token.Token, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DescribeTokenRequest not implemented")
}

func (s *service) QueryToken(ctx context.Context, req *token.QueryTokenRequest) (*token.TokenSet, error) {
	return nil, status.Errorf(codes.Unimplemented, "method QueryToken not implemented")
}
