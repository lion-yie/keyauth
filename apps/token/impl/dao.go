package impl

import (
	"context"
	"fmt"
	"gitee.com/lion-yie/keyauth/apps/token"
	"github.com/infraboard/mcube/exception"
	"github.com/infraboard/mcube/logger/zap"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func (s *service) save(ctx context.Context, ins *token.Token) error {
	// s.col.InsertMany()
	fmt.Println(ins)
	if _, err := s.col.InsertOne(ctx, ins); err != nil {
		zap.L().Error(err)
		return exception.NewInternalServerError("inserted token(%s) document error, %s",
			ins.AccessToken, err)
	}
	return nil
}

// filter:过滤器,类似于MySQL的Where条件
// 调用Decode方法来进行 反序列化 bytes->object(通过BSON tag)
func (s *service) get(ctx context.Context, accessToken string) (*token.Token, error) {
	filter := bson.M{"_id": accessToken}

	ins := token.NewDefaultToken()
	// 反序列化
	if err := s.col.FindOne(ctx, filter).Decode(ins); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exception.NewNotFound("access_token %s not found", accessToken)
		}

		return nil, exception.NewInternalServerError("find access_token %s error, %s", accessToken, err)
	}

	return ins, nil
}

func (s *service) deleteToken(ctx context.Context, ins *token.Token) error {
	if ins == nil || ins.AccessToken == "" {
		return fmt.Errorf("access_token is nil")
	}

	result, err := s.col.DeleteOne(ctx, bson.M{"_id": ins.AccessToken})
	if err != nil {
		return exception.NewInternalServerError("delete access_token(%s) error, %s", ins.AccessToken, err)
	}

	if result.DeletedCount == 0 {
		return exception.NewNotFound("access_token %s not found", ins.AccessToken)
	}

	return nil
}

func (s *service) update(ctx context.Context, ins *token.Token) error {
	if _, err := s.col.UpdateByID(ctx, ins.AccessToken, ins); err != nil {
		return exception.NewInternalServerError("update token(%s) document error, %s", ins.AccessToken, err)
	}
	return nil
}
