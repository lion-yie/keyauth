package all

import (
	_ "gitee.com/lion-yie/keyauth/apps/token/api"
	// 注册所有HTTP服务模块, 暴露给框架HTTP服务器加载
	_ "gitee.com/lion-yie/keyauth/apps/user/api"
)
